#!/usr/bin/env python
import rospy
from kobuki_msgs.msg import SensorState
from nav_msgs.msg import Odometry
from math import pi, cos, sin
from tf.transformations import quaternion_from_euler

from rospy.exceptions import ROSTimeMovedBackwardsException

class WheelOdom():
    def __init__(self):
        self.wheel_diameter = 0.070
        self.L = 0.230
        self.tick_per_rev = 1000
        self.last_left_encoder = 0
        self.last_right_encoder = 0
        self.last_update_time = 0
        self.is_init_value = False
        self.x = 0.0
        self.y = 0.0
        self.theta = 0.0

    def run(self):
        rospy.init_node("wheel_odom", anonymous=True)
        
        # Reset Encoder Count
        self.wheel_odom_pub = rospy.Publisher("/wheel_odom", Odometry, queue_size=10)
        self.sensor_sub = rospy.Subscriber("/mobile_base/sensors/core", SensorState, self.sensor_callback)
        rospy.spin()

    def sensor_callback(self,data):
        
        if not self.is_init_value:
            self.last_update_time = data.header.stamp.to_sec()
            self.last_left_encoder = data.left_encoder
            self.last_right_encoder = data.right_encoder
            self.is_init_value = True
        
        else:
            update_time = data.header.stamp.to_sec()
            dt = update_time - self.last_update_time
            self.last_update_time = update_time

            left_diff_tick = ((data.left_encoder + 32768 - self.last_left_encoder) % 65536) - 32768
            d_left_wheel = (2 * pi * (self.wheel_diameter / 2.0)) * (left_diff_tick / self.tick_per_rev)
            self.last_left_encoder = data.left_encoder

            right_diff_tick = ((data.right_encoder + 32768 - self.last_right_encoder) % 65536) - 32768
            d_right_wheel = (2 * pi * (self.wheel_diameter / 2.0)) * (right_diff_tick / self.tick_per_rev)
            self.last_right_encoder = data.right_encoder

            d_c = (d_left_wheel + d_right_wheel) / 2.0
            self.x += d_c * cos(self.theta)
            self.y += d_c * sin(self.theta)
            self.theta += (d_right_wheel - d_left_wheel) / self.L
            if self.theta > pi:
                self.theta = self.theta - (2 * pi)
            elif self.theta < -pi:
                self.theta = self.theta + (2 * pi)

            q = quaternion_from_euler(0.0, 0.0, self.theta)
        
            wheel_odom = Odometry()
            wheel_odom.header.stamp = rospy.Time.now()
            wheel_odom.pose.pose.position.x = self.x
            wheel_odom.pose.pose.position.y = self.y
            wheel_odom.pose.pose.orientation.x = q[0]
            wheel_odom.pose.pose.orientation.y = q[1]
            wheel_odom.pose.pose.orientation.z = q[2]
            wheel_odom.pose.pose.orientation.w = q[3]
        
            self.wheel_odom_pub.publish(wheel_odom)

if __name__ == '__main__':
    try:
        wheel_odom = WheelOdom()
        wheel_odom.run()
    except rospy.ROSInterruptException:
        pass